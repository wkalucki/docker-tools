package cleaners

interface Cleaner {

    void clean(CleaningOptions options)
}