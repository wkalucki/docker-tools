package cleaners

import groovy.cli.Option
import groovy.cli.Unparsed

class CleaningOptions {

    @Option(shortName = "r", longName = "regex", description = "Clean by regex")
    String regex

    @Option(shortName = "h", longName = "help", description = "Display help")
    Boolean help

    @Option(shortName = "e", longName = "exited", description = "Exited containers")
    Boolean exited

    @Option(shortName = "i", longName = "image", description = "Root image of container")
    String image

    @Unparsed
    List<String> resources

    @Option(shortName = "k", longName = "kill", description = "kill container if running")
    boolean kill

    @Option(shortName = "b", longName = "before", description = "Images before date")
    String before

    @Option(shortName = "a", longName = "after", description = "Images after date")
    String after


    String getRegex() {
        return regex
    }

    Boolean getHelp() {
        return help
    }

    List<String> getResources() {
        return resources
    }
}