package cleaners

import cleaners.containers.ContainerCleaner
import cleaners.images.ImageCleaner
@Grab('de.gesellix:docker-client:2019-12-15T21-43-13')
import de.gesellix.docker.client.DockerClientImpl
import groovy.cli.picocli.CliBuilder

import static cleaners.utils.CommunicationUtils.printInfo
import static cleaners.utils.CommunicationUtils.printError

class Handler {

    private final DockerClientImpl dockerClient
    private final Map<String, Cleaner> supportedResources;

    Handler() {
        this.dockerClient = new DockerClientImpl()
        this.supportedResources = ["images"    : new ImageCleaner(dockerClient.manageImage),
                                   "containers": new ContainerCleaner(dockerClient.manageContainer)

        ] as Map<String, Cleaner>

    }

    def validateArguments(List<String> arguments) {
        if (arguments != null && !arguments.isEmpty()) {
            for (String value : arguments) {
                if (!supportedResources.containsKey(value)) {
                    printError("Invalid positional argument:  " + value + "  please specify any of " + supportedResources.keySet())
                    return false
                }
            }
            return true
        } else {
            printError("Invalid positional argument, argument cannot be null or empty")
            return false
        }

    }


    def handle(CleaningOptions options, CliBuilder cliBuilder) {
        if (options.getHelp()) {
            println()
            cliBuilder.usage()
        } else if (!validateArguments(options.resources)) {
            System.exit(10)
        } else {
            verifyConnection()
            def choice = options.resources[0]
            supportedResources.get(choice).clean(options)
        }
    }

    private boolean verifyConnection() {
        def ping = dockerClient.ping()
        if (ping.status.success) {
            printInfo("Docker is up and running")
        }

    }
}


