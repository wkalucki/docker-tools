package cleaners.containers

class Container {

    String id
    String state
    String image
    List<String> mounts
    String displayName


    Container(String id, String state, String image, List<String> mounts) {
        this.id = id
        this.state = state
        this.image = image
        this.displayName = image + "  " + id
        this.mounts = mounts
    }


    @Override
    public String toString() {
        return "Container{" +
                "id='" + id + '\'' +
                ", state='" + state + '\'' +
                ", image='" + image + '\'' +
                ", mounts=" + mounts +
                ", displayName='" + displayName + '\'' +
                '}';
    }

    boolean equals(o) {
        if (this.is(o)) return true
        if (getClass() != o.class) return false

        Container container = (Container) o

        if (id != container.id) return false

        return true
    }

    int hashCode() {
        return id.hashCode()
    }
}
