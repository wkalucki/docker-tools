package cleaners.containers

import cleaners.Cleaner
import cleaners.CleaningOptions
import cleaners.deletion.Deleter
import de.gesellix.docker.client.container.ManageContainer

import static cleaners.utils.CommunicationUtils.printWarning
import static cleaners.utils.CommunicationUtils.warnIfEmpty

class ContainerCleaner implements Cleaner {

    ManageContainer manageContainer

    Deleter deleter

    Closure killing = { id ->
        return manageContainer.kill(id).status.success && manageContainer.rm(id).status.success
    }
    Closure removing = { id -> return manageContainer.rm(id).status.success }

    ContainerCleaner(ManageContainer manageContainer) {
        this.manageContainer = manageContainer
        this.deleter = new Deleter("containers")
    }

    @Override
    void clean(CleaningOptions options) {
        def containers = getAll()
        def operation = removing
        if (options.exited) {
            containers.removeIf({ container -> container.state != "exited" })
            warnIfEmpty(containers, "Could not find any exited containers")

        }
        if (options.image != null) {
            containers.removeIf({ container -> container.image != options.image })
            warnIfEmpty(containers, "Could not find any container created from image " + options.image)
        }
        if (options.kill) {
            printWarning("Running containers will be killed before removing")
            operation = killing
        }

        deleter.deleteMultiple(containers, operation)
    }

    private Set<Container> getAll() {
        Set<Container> containers = new HashSet<>()
        manageContainer.ps().content.each {
            def mounts = []
            it.get("Mounts").each { mount ->
                if (mount.Type == "volume") {
                    mounts.add(mount.Name)
                }

            }
            containers.add(new Container(it.getAt("Id"), it.get("State"), it.get("Image"), mounts))
        }
        containers
    }
}
