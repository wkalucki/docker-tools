package cleaners.deletion

import static cleaners.utils.CommunicationUtils.*

class Deleter {
    private String approvalTemplate = 'Are you sure u want to delete these %s? Y/N    '
    String type

    Deleter(String type) {
        this.type = type
    }


    int deleteMultiple(Set toDelete, DeletingOperation cleaningOperation) {
        def count = 0
        def allCount = toDelete.size()
        println()
        if (!toDelete.isEmpty()) {
            toDelete.each {
                println(it.displayName)
            }

            def requestApproval = String.format(approvalTemplate, type)
            def warning = getWarning(requestApproval)
            println(warning)

            String userInput = System.in.newReader().readLine()

            def ack = handleUserInput(userInput)

            if (ack) {
                toDelete.each {

                    def delete = cleaningOperation.delete(it.id)
                    if (delete) {
                        count++
                        printInfo("Successfully deleted " + type + "  " + count + "/" + allCount)
                    } else {
                        printError("Unable to delete " + type + " with id " + it)
                    }
                }
            }
        } else {
            printError("No " + type + " left to delete")
        }
        count
    }


}
