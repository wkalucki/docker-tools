package cleaners.deletion

@FunctionalInterface
interface DeletingOperation {

    boolean delete(String id)
}