package cleaners.images

import java.time.Instant

class Image {

    String id
    String tag
    Instant created
    String displayName

    Image(String id, String tag, Instant created) {
        this.id = id
        this.tag = tag
        this.created = created
        this.displayName = tag
    }

    @Override
    public String toString() {
        return "Image{" +
                "id='" + id + '\'' +
                ", tag='" + tag + '\'' +
                ", created=" + created +
                ", displayName='" + displayName + '\'' +
                '}';
    }

    boolean equals(o) {
        if (this.is(o)) return true
        if (getClass() != o.class) return false

        Image image = (Image) o

        if (id != image.id) return false

        return true
    }

    int hashCode() {
        return id.hashCode()
    }
}
