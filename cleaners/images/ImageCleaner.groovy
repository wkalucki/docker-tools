package cleaners.images

import cleaners.Cleaner
import cleaners.CleaningOptions
import cleaners.deletion.Deleter
import de.gesellix.docker.client.image.ManageImage

import java.time.Duration
import java.time.Instant
import java.text.SimpleDateFormat

import static cleaners.utils.CommunicationUtils.warnIfEmpty

class ImageCleaner implements Cleaner {

    private final ManageImage manageImage
    private Deleter deleter
    private static final boolean POSITIVE = true
    private static final boolean NEGATIVE = false


    ImageCleaner(ManageImage manageImage) {
        this.manageImage = manageImage
        this.deleter = new Deleter("images")
    }

    private Set getAll() {
        Set<Image> images = new HashSet<Image>()
        manageImage.images().content.each {
            images.add(new Image(it.get("Id"), it.get("RepoTags")[0], Instant.ofEpochSecond((long) it.get("Created"))))
        }
        images
    }


    void clean(CleaningOptions cleaningOptions) {

        Set<Image> images = getAll()
        if (cleaningOptions.regex != null) {
            filterByRegex(images, cleaningOptions.regex)
        }
        if (cleaningOptions.before != null) {
            filterByTime(cleaningOptions.before, images, NEGATIVE)
        }
        if (cleaningOptions.after != null) {
            filterByTime(cleaningOptions.after, images, POSITIVE)
        }

        delete(images)
    }

    private void filterByRegex(HashSet<Image> images, String regex) {
        images.removeIf({ image -> !(image.tag ==~ regex) })
        warnIfEmpty(images, "No images matched the specified regex " + regex)

    }

    private void filterByTime(String time, HashSet<Image> images, boolean expected) {

        def param = new SimpleDateFormat("yyyy-MM-dd").parse(time).toInstant()
        images.removeIf({ image ->
            Duration duration = Duration.between(image.created, param)
            return duration.isPositive() == expected
        })
        warnIfEmpty(images, "No images matched specified time filter " + param)
    }

    private void delete(HashSet<Image> images) {
        deleter.deleteMultiple(images, { id -> return manageImage.rmi(id).status.success })
    }
}