package cleaners.utils

class CommunicationUtils {

    static final String NORMAL = "\u001B[0m"
    static final String RED = "\u001B[31m"
    static final String GREEN = "\u001B[32m"
    static final String YELLOW = "\u001B[33m"

    static printError(String text) {
        println(RED + text + NORMAL)

    }

    static void printWarning(String text) {
        println(YELLOW + text + NORMAL)
    }

    static String getWarning(String text) {
        def warning = YELLOW + text + NORMAL
        warning

    }

    static printInfo(String text) {
        println(GREEN + text + NORMAL)

    }

    static boolean handleUserInput(String s) {
        return s.toLowerCase() == 'y'
    }

    static void warnIfEmpty(Set collection, String text) {
        if (collection.isEmpty()) {
            printWarning(text)
        }
    }
}
