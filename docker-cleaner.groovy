import cleaners.CleaningOptions
import cleaners.Handler

import groovy.cli.picocli.CliBuilder

CliBuilder cliBuilder = new CliBuilder(name: 'Docker resources cleaner')

def options = cliBuilder.parseFromInstance(new CleaningOptions(), this.args)

def handler = new Handler()

handler.handle(options, cliBuilder)
